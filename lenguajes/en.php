<?php 
/* Idioma Actual */
$lenguaje = 'es';

/* Ajustes del Sitio */
$name = 'KA Development';
$title = 'Ideas de Café';
$description = 'Un blog de programación y personalidad';

/* Barra de navegación */
$home = 'Home';
$about = '¿About KA?';
$user = 'In';
$contact = 'Contact';

/* ¿Qué es KA? */
$our_company = 'Nuestra Compañía';
$we_make = 'Capacita, Desarrolla e Innova';
$we_descrption = 'Nosotros hacemos esto y aquello';

/* Contacto */
$contact_title = 'Contáctenos';
$contact_descrption = '¿Tener preguntas? Tengo respuestas (quizás)';
$contact_form = '¿Quieres ponerte en contacto con nosotros? ¡Complete el formulario abajo para enviarme un mensaje y trataremos de responderle dentro de 24 horas!';

$cf_name = 'Nombres';
$cf_name_message = 'Por favor ingrese su nombre';
$cf_email = 'Correo Electrónico';
$cf_email_message = 'Por favor ingrese su dirección de correo electrónico';
$cf_message = 'Mensaje';
$cf_message_message = 'Por favor ingrese su mensaje';

$cf_send = 'Enviar';

/* Redes Sociales */
$twitter = '';
$facebook = '';
$git = '';

?>