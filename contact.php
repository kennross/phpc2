<?php 
session_start();

if(isset($_SESSION['idioma_actual'])) {
	if ($_SESSION['idioma_actual'] == 'spanish') {
		include('lenguajes/es.php');
	} else {
		include('lenguajes/en.php');
	}
} else {
	include('lenguajes/es.php');
}
?>
<!DOCTYPE html>
<html lang="<?php echo $lenguaje ?>">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>
		<?php echo $title ?>
	</title>

	<!-- Bootstrap Core CSS -->
	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Theme CSS -->
	<link href="css/clean-blog.min.css" rel="stylesheet">

	<!-- Custom Fonts -->
	<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
</head>

<body>

	<?php include('parts/nav.php') ?>

	<!-- Page Header -->
	<!-- Set your background image for this header on the line below. -->
	<header class="intro-header" style="background-image: url('img/contact-bg.jpg')">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
					<div class="page-heading">
						<h1>
							<?php echo $contact_title ?>
						</h1>
						<hr class="small">
						<span class="subheading">
							<?php echo $contact_descrption ?>
						</span>
					</div>
				</div>
			</div>
		</div>
	</header>

	<!-- Main Content -->
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
				<p>
				<?php echo $contact_form ?>
				</p>

				<form name="sentMessage" id="contactForm" novalidate>
					<div class="row control-group">
						<div class="form-group col-xs-12 floating-label-form-group controls">
							<label>
								<?php echo $cf_name ?>
							</label>
							<input type="text" class="form-control" placeholder="<?php echo $cf_name ?>" id="name" required data-validation-required-message="<?php echo $cf_name_message ?>">
							<p class="help-block text-danger"></p>
						</div>
					</div>
					<div class="row control-group">
						<div class="form-group col-xs-12 floating-label-form-group controls">
							<label>
								<?php echo $cf_email ?>
							</label>
							<input type="email" class="form-control" placeholder="<?php echo $cf_email ?>" id="email" required data-validation-required-message="<?php echo $cf_email_message ?>">
							<p class="help-block text-danger"></p>
						</div>
					</div>					
					<div class="row control-group">
						<div class="form-group col-xs-12 floating-label-form-group controls">
							<label>
								<?php echo $cf_message ?>
							</label>
							<textarea rows="5" class="form-control" placeholder="<?php echo $cf_message ?>" id="message" required data-validation-required-message="<?php echo $cf_message_message ?>"></textarea>
							<p class="help-block text-danger"></p>
						</div>
					</div>
					<br>
					<div id="success"></div>
					<div class="row">
						<div class="form-group col-xs-12">
							<button type="submit" class="btn btn-default">
								<?php echo $cf_send ?>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<hr>

	<?php include('parts/footer.php') ?>

	<!-- jQuery -->
	<script src="vendor/jquery/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Contact Form JavaScript -->
	<script src="js/jqBootstrapValidation.js"></script>
	<script src="js/contact_me.js"></script>

	<!-- Theme JavaScript -->
	<script src="js/clean-blog.min.js"></script>

</body>

</html>
