<!-- Navigation -->
<nav class="navbar navbar-default navbar-custom navbar-fixed-top">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header page-scroll">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">
					Toggle navigation
				</span>
				Menu &nbsp;
				<i class="fa fa-bars"></i>
			</button>
			<a class="navbar-brand" href="index.php">
				<?php echo $name ?>
			</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				<li <?php if (basename($_SERVER['PHP_SELF']) == 'index.php') {echo 'class="active"';} ?>>
					<a href="index.php">
						<?php echo $home ?>
					</a>
				</li>
				<li <?php if (basename($_SERVER['PHP_SELF']) == 'about.php') {echo 'class="active"';} ?>>
					<a href="about.php">
						<?php echo $about ?>
					</a>
				</li>
								
				<li <?php if (basename($_SERVER['PHP_SELF']) == 'contact.php') {echo 'class="active"';} ?>>
					<a href="contact.php">
						<?php echo $contact ?>
					</a>
				</li>
				<?php if (!isset($_SESSION['miperfil'])) { ?>
				<li <?php if (basename($_SERVER['PHP_SELF']) == 'user.php') {echo 'class="active"';} ?>>
					<a href="user.php">
						<?= $user ?>
					</a>
				</li>
				<?php } ?>
				<?php if (isset($_SESSION['miperfil'])) { ?>
				<li <?php if (basename($_SERVER['PHP_SELF']) == 'miperfil.php') {echo 'class="active"';} ?>>
					<a href="miperfil.php">
						<?= $_SESSION['miperfil']['nombre'] ?>
					</a>
				</li>
				<li>
					<a href="controladores/salir.php">
						<?= 'Salir' ?>
					</a>
				</li>
				<?php } ?>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container -->
</nav>